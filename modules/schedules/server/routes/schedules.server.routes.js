'use strict';

/**
 * Module dependencies.
 */
var schedulesPolicy = require('../policies/schedules.server.policy'),
  schedules = require('../controllers/schedules.server.controller');

module.exports = function (app) {
  // Schedules collection routes
  app.route('/api/schedules').all(schedulesPolicy.isAllowed)
    .get(schedules.list)
    .post(schedules.create);

  app.route('/api/schedules/mockDevice').all(schedulesPolicy.isAllowed)
    .get(schedules.readdevice);

  app.route('/api/schedules/mockDoctor').all(schedulesPolicy.isAllowed)
    .get(schedules.readdoctor);

  // Single article routes
  app.route('/api/schedules/:scheduleId').all(schedulesPolicy.isAllowed)
    .get(schedules.read)
    .put(schedules.update)
    .delete(schedules.delete);

  // Finish by binding the article middleware
  app.param('scheduleId', schedules.scheduleByID);
};
