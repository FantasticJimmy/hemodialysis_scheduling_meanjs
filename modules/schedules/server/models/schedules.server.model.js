'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Article Schema
 */
var ScheduleSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  doctor: {
    type: String,
  },
  device:{
    type: String,
  },
  user:{
    type: Schema.ObjectId,
    ref:'User'
  },
  startTime: {
    type: Date,
    default: Date.now
  },
  endTime:{
    type: Date,
    default: Date.now
  },
  sessionSymbol:{
    type: String
  }
});

mongoose.model('Schedule', ScheduleSchema);
