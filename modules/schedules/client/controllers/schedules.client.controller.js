'use strict';

// Schedule controller
angular.module('schedules').controller('SchedulesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Schedules','Articles','$http','$q',
  function ($scope, $stateParams, $location, Authentication, Schedules,Articles,$http,$q) {
    // $scope.authentication = Authentication;


    $scope.selectedSchedule = { date:'',device:'',sessionSymbol:'' };


    $scope.find = function(){
      Schedules.query({},function(data){
        $scope.schedules = data;

        $q.all({
          doctor:$http.get('api/schedules/mockDoctor'),
          devices:$http.get('api/schedules/mockDevice')
        }).then(
          function(response){
            $scope.devices = response.devices.data;
            for(var i=0;i<$scope.devices.length;i++){
              if($scope.devices[i].active){
                $scope.devices[i].dateArray = [];
                for(var j=0;j<7;j++){
                  $scope.devices[i].dateArray.push({ bookedAm: false,bookedPm:false,date:new Date(new Date().setHours(0,0,0,0)+j*24*60*60*1000) });
                  for(var k = 0;k < $scope.schedules.length;k++){
                    var bookedSchedule = new Date($scope.schedules[k].startTime);
                    if(parseInt($scope.schedules[k].device) === $scope.devices[i]._id){
                      if(bookedSchedule.getDate() === $scope.devices[i].dateArray[j].date.getDate()){
                        if(bookedSchedule.getHours()<=12){
                          $scope.devices[i].dateArray[j].bookedAm = true;
                        }
                        if(bookedSchedule.getHours() > 12){
                          $scope.devices[i].dateArray[j].bookedPm = true;
                        }
                      }
                    }
                  }
                }
              }
            }
          });
      });
    };


    $scope.selectIt = function(selection,session, device){
      if(session === 'A'){
        $scope.selectedSchedule.date = new Date(selection.date.setHours(8,0,0,0));
      }
      if(session === 'P'){
        $scope.selectedSchedule.date = new Date(selection.date.setHours(13,0,0,0));
      }
      $scope.selectedSchedule.sessionSymbol = session;
      $scope.selectedSchedule.device = device;
    };

    // $scope.seek = function () {
    //   var schedule = new Schedules();
    //   $q.all({
    //     doctor:$http.get('api/schedules/mockDoctor'),
    //     devices:$http.get('api/schedules/mockDevice')
    //   }).then(function(res){
    //     schedule.doctor = res.doctor.data._id;
    //     // schedule.device = res.devices.data.devices[0]._id;
    //     schedule.device = "3";
    //     var nowDate = new Date().getDate();
    //     var nowTime = new Date().getHours();
    //     if(nowTime <= 12){
    //       schedule.startTime = new Date(new Date().setHours(13,0,0,0));
    //       schedule.endTime = new Date(new Date().setHours(17,0,0,0));
    //     }else{
    //       schedule.startTime = new Date(new Date().setHours(8,0,0,0)+24 * 60 * 60 * 1000);
    //       schedule.endTime = new Date(new Date().setHours(12,0,0,0)+24 * 60 * 60 * 1000);
    //     }
    //     schedule.sessionSymbol = "A";
    //     schedule.$save(function(response){
    //       $scope.schedule = response;
    //     });
    //   });
    // };  

    $scope.saveIt = function(){
      var schedule = new Schedules();
      schedule.device = $scope.selectedSchedule.device;
      schedule.sessionSymbol = $scope.selectedSchedule.sessionSymbol;
      schedule.startTime = $scope.selectedSchedule.date;
      schedule.endTime = new Date($scope.selectedSchedule.date.getTime()+4*60*60*1000);
      schedule.doctor = '1';
      delete schedule.date;
      schedule.$save(function (response) {
        $scope.schedule = response;
        location.reload();
      });
    };
    //   // Create new Schedule object
    //   var schedule = new Schedules({
    //     title: this.title,
    //     content: this.content
    //   });


    //     // Clear form fields
    //     $scope.title = '';
    //     $scope.content = '';
    //   }, function (errorResponse) {
    //     $scope.error = errorResponse.data.message;
    //   });
    // };

    // // Remove existing Schedule
    // $scope.remove = function (schedule) {
    //   if (schedule) {
    //     schedule.$remove();

    //     for (var i in $scope.schedules) {
    //       if ($scope.schedules[i] === schedule) {
    //         $scope.schedules.splice(i, 1);
    //       }
    //     }
    //   } else {
    //     $scope.schedule.$remove(function () {
    //       $location.path('schedules');
    //     });
    //   }
    // };

    // // Update existing Schedule
    // $scope.update = function (isValid) {
    //   $scope.error = null;

    //   if (!isValid) {
    //     $scope.$broadcast('show-errors-check-validity', 'scheduleForm');

    //     return false;
    //   }

    //   var schedule = $scope.schedule;

    //   schedule.$update(function () {
    //     $location.path('schedules/' + schedule._id);
    //   }, function (errorResponse) {
    //     $scope.error = errorResponse.data.message;
    //   });
    // };

    // // Find a list of Schedules
    // $scope.find = function () {
    //   $scope.count = 2;
    //   $scope.schedules = Schedules.query();
    // };

    // // Find existing Schedule
    // $scope.findOne = function () {
    //   $scope.schedule = Schedules.get({
    //     scheduleId: $stateParams.scheduleId
    //   });
    // };
  }
]);

