'use strict';

// Configuring the Articles module
angular.module('schedules').run(['Menus',
  function (Menus) {
    // Add the articles dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Schedules',
      state: 'schedules',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'schedules', {
      title: 'My Schedules',
      state: 'schedules.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'schedules', {
      title: 'Seek Schedule',
      state: 'schedules.seek',
      roles: ['user']
    });
  }
]);