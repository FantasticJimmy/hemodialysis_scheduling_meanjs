'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$state', 'Authentication', 'Menus','gettextCatalog',
  function ($scope, $state, Authentication, Menus,gettextCatalog) {
    // Expose view variables
    $scope.$state = $state;
    $scope.authentication = Authentication;

    // Get the topbar menu
    $scope.menu = Menus.getMenu('topbar');

    // Toggle the menu items
    $scope.isCollapsed = false;
    $scope.toggleCollapsibleMenu = function () {
      $scope.isCollapsed = !$scope.isCollapsed;
    };

    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function () {
      $scope.isCollapsed = false;
    });
    $scope.english = true;
    //Toggle languageb betwee Chinese and English
    var languageFunc = function(english){
      if(english){
        gettextCatalog.setCurrentLanguage('en');
      }else{
        gettextCatalog.setCurrentLanguage('zh_CN');
      }
    };
    $scope.chengeLanguage = function(){
      $scope.english = !$scope.english;
      languageFunc($scope.english);
    };
    languageFunc($scope.english);
  }
]);